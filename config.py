import pygame

# create the screen

pygame.display.set_caption("OBSTACLES")

# player1
player1_active = 1
player1Img = pygame.image.load('player1.png')
player1X = 370
player1Y = 568
player1X_change = 0

# Back ground
bgi = pygame.image.load('bgi.png')

# captions
player1Y_change = 0

# player2
player2_active = 0
player2Img = pygame.image.load('player2.png')
player2X = 370
player2Y = 0
player2X_change = 0
player2Y_change = 0
winner = 0
