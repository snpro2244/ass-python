from config import *
from pygame import mixer

# Initialise the pygame
pygame.init()
screen = pygame.display.set_mode((800, 600))

# Enemy
enemyImg = []
enemyX = []
enemyY = []
enemyX_change = []
num_of_enemies = 4
temp = 58
for i in range(num_of_enemies):
    enemyImg.append(pygame.image.load('enemy.png'))
    enemyX.append(0)
    enemyY.append(temp)
    enemyX_change.append(2)
    temp += 140
enemyX[1] = 180
enemyX[3] = 500

# Fixed obstacles
obsImg = []
obsX = []
obsY = []
num_of_obs = 3
for j in range(num_of_obs):
    obsImg.append(pygame.image.load('obs.png'))
    if j == 0:
        obsX.append(370)
        obsY.append(420)
    elif j == 1:
        obsX.append(500)
        obsY.append(280)
    elif j == 2:
        obsX.append(240)
        obsY.append(140)

# Score

score_value1 = 0
font = pygame.font.Font('freesansbold.ttf', 16)
scoreX1 = 10
scoreY1 = 10

temp1 = 0
temp2 = 0

score_value2 = 0
scoreX2 = 700
scoreY2 = 10

# rounds
round1 = 1
round2 = 0


def player1(x, y):
    screen.blit(player1Img, (x, y))


def player2(x, y):
    screen.blit(player2Img, (x, y))


def enemy(x, y, h):
    screen.blit(enemyImg[h], (x, y))


def obs(x, y, h):
    screen.blit(obsImg[h], (x, y))


def iscollision1(ex, ey, px, py):
    y1 = ey
    y2 = ey + 64
    x1 = ex
    x2 = ex + 64
    if x1 <= px <= x2 and y2 >= py >= y1:
        return True
    elif x1 <= px <= x2 and y2 >= py + 32 >= y1:
        return True
    else:
        return False


def iscollissionobs1(obsx, obsy, px, py):
    if (obsx - 32 <= px <= obsx + 32) and (obsy <= py <= obsy + 32 or obsy <= py + 32 <= obsy + 32):
        return True
    else:
        return False


def iscollision2(enemyX, enemyY, playerX, playerY):
    y1 = enemyY
    y2 = enemyY + 64
    x1 = enemyX
    x2 = enemyX + 64
    if x1 <= playerX <= x2 and y2 >= playerY + 32 >= y1:
        return True
    elif x1 <= playerX <= x2 and y2 >= playerY >= y1:
        return True
    else:
        return False


def iscollissionobs2(obsx, obsy, px, py):
    if (obsx - 32 <= px <= obsx + 32) and (obsy <= py <= obsy + 32 or obsy <= py + 32 <= obsy + 32):
        return True
    else:
        return False


def show_score1(x, y):
    score1 = font.render("SCORE1 :" + str(score_value1), True, (0, 255, 0))
    screen.blit(score1, (x, y))


def show_score2(x, y):
    score2 = font.render("SCORE2 :" + str(score_value2), True, (0, 180, 180))
    screen.blit(score2, (x, y))


def show_round(state):
    r = font.render("ROUND :" + str(state), True, (0, 180, 180))
    screen.blit(r, (10, 570))


ind_respawn = 0
winner_r1 = 0
time1 = 0
time2 = 0
# /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# Game Loop
running = True
while running:
    screen.blit(bgi, (0, 0))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        # pressing
        if event.type == pygame.KEYDOWN:
            if player1_active == 1:
                if event.key == pygame.K_LEFT:
                    player1X_change = -2
                if event.key == pygame.K_RIGHT:
                    player1X_change = 2
                if event.key == pygame.K_DOWN:
                    player1Y_change = 2
                if event.key == pygame.K_UP:
                    player1Y_change = -2
            if player2_active == 1:
                if event.key == pygame.K_a:
                    player2X_change = -2
                if event.key == pygame.K_d:
                    player2X_change = 2
                if event.key == pygame.K_s:
                    player2Y_change = 2
                if event.key == pygame.K_w:
                    player2Y_change = -2

        # releasing
        if event.type == pygame.KEYUP:
            if player1_active == 1:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    player1X_change = 0

                if event.key == pygame.K_DOWN or event.key == pygame.K_UP:
                    player1Y_change = 0

            if player2_active == 1:
                if event.key == pygame.K_a or event.key == pygame.K_d:
                    player2X_change = 0
                if event.key == pygame.K_s or event.key == pygame.K_w:
                    player2Y_change = 0
    # constraints for player1
    if player1X >= 768:
        player1X = 768
    if player1X <= 0:
        player1X = 0
    if player1Y >= 568:
        player1Y = 568
    # constraints for player2
    if player2X <= 0:
        player2X = 0
    if player2X >= 768:
        player2X = 768
    if player2Y <= 0:
        player2Y = 0

    # obstacle
    for j in range(num_of_obs):
        obs(obsX[j], obsY[j], j)
        if player1_active == 1:
            collision = iscollissionobs1(obsX[j], obsY[j], player1X, player1Y)
            if collision:
                explosionSound = mixer.Sound("laser.wav")
                explosionSound.play()
                player2_active = 1
                player1_active = 0
                player1Y_change = 0
                player1X_change = 0
                player2X = 370
                player2Y = 0
                player2Y_change = 0
                player2X_change = 0
        if player2_active == 1:
            collision = iscollissionobs2(obsX[j], obsY[j], player2X, player2Y)
            if collision:
                explosionSound = mixer.Sound("laser.wav")
                explosionSound.play()
                ind_respawn += 1
                player2_active = 0
                player2X_change = 0
                player2Y_change = 0
                player1_active = 1
                player1X = 370
                player1Y = 568
                player1Y_change = 0
                player1X_change = 0
                if round1 == 1:
                    round1 = 0
                    round2 = 1
                    temp1 = score_value1
                    temp2 = score_value2
                if round2 == 1 and ind_respawn == 2:
                    run1 = True
                    s1 = font.render("SCORE OF PLAYER1 :" + str(score_value1), True, (0, 255, 0))
                    s2 = font.render("SCORE OF PLAYER2 :" + str(score_value2), True, (0, 255, 0))
                    s11 = font.render("Time OF PLAYER 1 :" + str(time1) + "units", True, (0, 255, 0))
                    s22 = font.render("Time OF PLAYER 2 :" + str(time2) + "units", True, (0, 255, 0))
                    if score_value1 > score_value2:
                        winner = 1
                        s33 = font.render("winner is player1", True, (0, 255, 0))
                    elif score_value1 < score_value2:
                        winner = 2
                        s33 = font.render("winner is player2", True, (0, 255, 0))
                    else:
                        if time1 > time2:
                            winner = 2
                            s33 = font.render("winner is player2", True, (0, 255, 0))
                        elif time1 < time2:
                            winner = 1
                            s33 = font.render("winner is player1", True, (0, 255, 0))
                        else:
                            winner = 0
                            s33 = font.render("its a tie", True, (0, 255, 0))

                    while run1:
                        screen.fill((100, 0, 0))
                        screen.blit(s1, (300, 140))
                        screen.blit(s2, (300, 420))
                        screen.blit(s11, (300, 180))
                        screen.blit(s22, (300, 340))
                        screen.blit(s33, (300, 30))
                        for event in pygame.event.get():
                            if event.type == pygame.QUIT:
                                run1 = False
                                running = False
                                pygame.display.update()
                        pygame.display.update()
    if not running:
        break

    # Enemy Movement
    for i in range(num_of_enemies):
        if round2 == 1:
            enemyX_change[i] = 6
        enemyX[i] += enemyX_change[i]

        if enemyX[i] >= 736:
            enemyX[i] = 0
        enemy(enemyX[i], enemyY[i], i)
        if player1_active == 1:
            collision = iscollision1(enemyX[i], enemyY[i], player1X, player1Y)
            if collision:
                explosionSound = mixer.Sound("explosion.wav")
                explosionSound.play()
                player2_active = 1
                player1_active = 0
                player1Y_change = 0
                player1X_change = 0
                player2X = 370
                player2Y = 0
                player2Y_change = 0
                player2X_change = 0
        elif player2_active == 1:
            collision = iscollision2(enemyX[i], enemyY[i], player2X, player2Y)
            if collision:
                explosionSound = mixer.Sound("explosion.wav")
                explosionSound.play()
                ind_respawn += 1
                player2_active = 0
                player2X_change = 0
                player2Y_change = 0
                player1_active = 1
                player1X = 370
                player1Y = 568
                player1Y_change = 0
                player1X_change = 0
                if round1 == 1:
                    round1 = 0
                    round2 = 1
                    temp1 = score_value1
                    temp2 = score_value2

                if round2 == 1 and ind_respawn == 2:
                    run1 = True
                    s1 = font.render("SCORE OF PLAYER 1 :" + str(score_value1), True, (0, 255, 0))
                    s11 = font.render("Time OF PLAYER 1 :" + str(time1) + "units", True, (0, 255, 0))
                    s2 = font.render("SCORE OF PLAYER 2 :" + str(score_value2), True, (0, 255, 0))
                    s22 = font.render("Time OF PLAYER 2 :" + str(time2) + "units", True, (0, 255, 0))
                    if score_value1 > score_value2:
                        winner = 1
                        s33 = font.render("winner is player1", True, (0, 255, 0))
                    elif score_value1 < score_value2:
                        winner = 2
                        s33 = font.render("winner is player2", True, (0, 255, 0))
                    else:
                        if time1 > time2:
                            winner = 2
                            s33 = font.render("winner is player2", True, (0, 255, 0))
                        elif time1 < time2:
                            winner = 1
                            s33 = font.render("winner is player1", True, (0, 255, 0))
                        else:
                            winner = 0
                            s33 = font.render("its a tie", True, (0, 255, 0))
                    while run1:
                        screen.fill((100, 0, 0))
                        screen.blit(s1, (300, 140))
                        screen.blit(s2, (300, 300))
                        screen.blit(s1, (300, 140))
                        screen.blit(s11, (300, 180))
                        screen.blit(s22, (300, 340))
                        screen.blit(s33, (300, 30))
                        for event in pygame.event.get():
                            if event.type == pygame.QUIT:
                                run1 = False
                                running = False
                                pygame.display.update()
                        pygame.display.update()
    if not running:
        break

    # updating
    if player1_active:
        player1Y += player1Y_change
        player1X += player1X_change
    if player2_active:
        player2Y += player2Y_change
        player2X += player2X_change
    # RESPAWING
    if player1Y <= 0:
        player1_active = 0
        player2_active = 1
        player1X = 370
        player1Y = 568
        player2Y = 0
        player2X = 370
        player1X_change = 0
        player1Y_change = 0

    if player2Y >= 568:
        player1_active = 1
        player2_active = 0
        ind_respawn += 1
        player2X = 370
        player2Y = 0
        player1Y = 568
        player1X = 370
        player2X_change = 0
        player2Y_change = 0
        if round1 == 1:
            round1 = 0
            round2 = 1

            temp1 = 55
            temp2 = 55

        if round2 == 1 and ind_respawn == 2:
            run1 = True
            s1 = font.render("SCORE OF PLAYER1 :" + str(score_value1), True, (0, 255, 0))
            s2 = font.render("SCORE OF PLAYER2 :" + str(score_value2), True, (0, 255, 0))
            s11 = font.render("Time OF PLAYER 1 :" + str(time1) + "units", True, (0, 255, 0))
            s22 = font.render("Time OF PLAYER 2 :" + str(time2) + "units", True, (0, 255, 0))
            if score_value1 > score_value2:
                winner = 1
                s33 = font.render("winner is player1", True, (0, 255, 0))
            elif score_value1 < score_value2:
                winner = 2
                s33 = font.render("winner is player2", True, (0, 255, 0))
            else:
                if time1 > time2:
                    winner = 2
                    s33 = font.render("winner is player2", True, (0, 255, 0))
                elif time1 < time2:
                    winner = 1
                    s33 = font.render("winner is player1", True, (0, 255, 0))
                else:
                    winner = 0
                    s33 = font.render("its a tie", True, (0, 255, 0))
            while run1:
                screen.fill((100, 0, 0))
                screen.blit(s1, (300, 140))
                screen.blit(s2, (300, 420))
                screen.blit(s11, (300, 180))
                screen.blit(s22, (300, 340))
                screen.blit(s33, (300, 30))
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        run1 = False
                        running = False
                        pygame.display.update()
                pygame.display.update()

    # SCORING
    if player1_active and round1:
        # if 428 >= player1Y > 290 and score_value1 == 0:
        if 440 <= player1Y <= 446 and score_value1 == 0:
            score_value1 = 10
        if 387 <= player1Y <= 392 and score_value1 == 10:
            score_value1 = 15
        if 300 <= player1Y <= 306 and score_value1 == 15:
            score_value1 = 25
        if 248 <= player1Y <= 252 and score_value1 == 25:
            score_value1 = 30
        if 162 <= player1Y <= 166 and score_value1 == 30:
            score_value1 = 40
        if 108 <= player1Y <= 112 and score_value1 == 40:
            score_value1 = 45
        if 22 <= player1Y <= 26 and score_value1 == 45:
            score_value1 = 55

    if player2_active and round1:
        if 122 <= player2Y < 126 and score_value2 == 0:
            score_value2 = 10
        if 176 <= player2Y < 182 and score_value2 == 10:
            score_value2 = 15
        if 262 <= player2Y < 266 and score_value2 == 15:
            score_value2 = 25
        if 316 <= player2Y < 320 and score_value2 == 25:
            score_value2 = 30
        if 402 <= player2Y < 406 and score_value2 == 30:
            score_value2 = 40
        if 456 <= player2Y < 460 and score_value2 == 40:
            score_value2 = 45
        if 542 <= player2Y < 546 and score_value2 == 45:
            score_value2 = 55
    if player1_active and round2:
        if 440 <= player1Y <= 446:
            score_value1 = 10 + temp1
        if 387 <= player1Y <= 392:
            score_value1 = 15 + temp1
        if 300 <= player1Y <= 306:
            score_value1 = 25 + temp1
        if 248 <= player1Y <= 252:
            score_value1 = 30 + temp1
        if 162 <= player1Y <= 166:
            score_value1 = 40 + temp1
        if 108 <= player1Y <= 112:
            score_value1 = 45 + temp1
        if 22 <= player1Y <= 26:
            score_value1 = 55 + temp1

    if player2_active and round2:
        if 122 <= player2Y < 126:
            score_value2 = 10 + temp2
        if 176 <= player2Y < 182:
            score_value2 = 15 + temp2
        if 262 <= player2Y < 266:
            score_value2 = 25 + temp2
        if 316 <= player2Y < 320:
            score_value2 = 30 + temp2
        if 402 <= player2Y < 406:
            score_value2 = 40 + temp2
        if 456 <= player2Y < 460:
            score_value2 = 45 + temp2
        if 542 <= player2Y < 546:
            score_value2 = 55 + temp2
    if not running:
        break
    # displaying
    if player1_active:
        player1(player1X, player1Y)
    if player2_active:
        player2(player2X, player2Y)
    show_score1(scoreX1, scoreY1)
    show_score2(scoreX2, scoreY2)
    if round1 == 1:
        show_round(1)
    elif round2 == 1:
        show_round(2)
    if player1_active == 1:
        time1 += 1
    if player2_active == 1:
        time2 += 1
    pygame.display.update()
